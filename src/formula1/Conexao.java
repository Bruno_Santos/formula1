package formula1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    public static Connection obterConexao() {
        try {
            Class.forName("org.postgresql.Driver");
            String usuario = "bruno";
            String senha = "bruno";
            String banco = "jdbc:postgresql://10.90.24.54/bruno";
            return DriverManager.getConnection(banco, usuario, senha);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
