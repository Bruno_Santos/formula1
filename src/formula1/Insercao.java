/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formula1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class Insercao {
    public void Inserir(String nome, String numeroinicial, String posicaocampeonato, String equipe){
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_Formula1.Piloto(nome, numeroinicial, posicaocampeonato, equipe) values (?, ?, ?, ?)");
            ps.setString(1, nome);
            ps.setInt(2, Integer.valueOf( numeroinicial));
            ps.setInt(3, Integer.valueOf(posicaocampeonato));
            ps.setString(4, equipe);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirCorrida(String data, String nomecircuito, String numvoltas, String premio){
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_Formula1.Corrida(data, nomecircuito, numvoltas, premio) values (?, ?, ?, ?)");
            ps.setString(1, data);
            ps.setString(2, nomecircuito);
            ps.setInt(3, Integer.valueOf(numvoltas));
            ps.setDouble(4, Double.valueOf(premio));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirFormula(String nome, int numeroinicial, String modelo, String motor){
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_formula1.formula(nome, numeroinicial, modelo, motor) values (?, ?, ?, ?)");
            ps.setString(1, nome);
            ps.setInt(2,  numeroinicial);
            ps.setString(3, modelo);
            ps.setString(4, motor);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
