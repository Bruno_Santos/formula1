package formula1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

public class ListarCorrida {
    public void listar(JList listCorrida){
        try {
            listCorrida.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String sql = "select * from sc_Formula1.Corrida";
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery(sql);
            while(rs.next()){
                dfm.addElement(rs.getString("nomecircuito"));
            }
            listCorrida.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarCorrida.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
