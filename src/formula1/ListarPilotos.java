/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formula1;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class ListarPilotos {
    public void listar(JList listPilotos){
        try {
            listPilotos.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String sql = "select * from sc_Formula1.Piloto";
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery(sql);
            while(rs.next()){
                dfm.addElement(rs.getString("nome"));
            }
            listPilotos.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarPilotos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void selecaoPiloto(JComboBox combo){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.obterConexao();
            PreparedStatement p = c.prepareStatement("select * from sc_formula1.piloto");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
                Piloto pil = new Piloto(rs.getString("nome"), rs.getInt("posicaocampeonato"), rs.getString("equipe"), rs.getInt("numeroinicial"));
                m.addElement(pil);
            } 
            combo.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarPilotos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
