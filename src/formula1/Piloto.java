package formula1;

public class Piloto {
    private String nome;
    private int posicaocampeonato;
    private String equipe;
    private int numeroinicial;

    @Override
    public String toString() {
        return this.nome;
    }

    public Piloto(String nome, int posicaocampeonato, String equipe, int numeroinicial) {
        this.nome = nome;
        this.posicaocampeonato = posicaocampeonato;
        this.equipe = equipe;
        this.numeroinicial = numeroinicial;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPosicaocampeonato() {
        return posicaocampeonato;
    }

    public void setPosicaocampeonato(int posicaocampeonato) {
        this.posicaocampeonato = posicaocampeonato;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public int getNumeroinicial() {
        return numeroinicial;
    }

    public void setNumeroinicial(int numeroinicial) {
        this.numeroinicial = numeroinicial;
    }
    
}
